﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace HWParallel
{
    class Program
    {
        private static int[] ints = new int[1_000_000];

        static void Main(string[] args)
        {
            AddInts();

            GetSum();
            GetSumParallelFor();
            GetSumTask();
            GetSumParallelLinq();

            Console.ReadKey();
        }

        private static void AddInts()
        {
            for (int i = 0; i < 1_000_000; i++)
                ints[i] = 2;
        }

        private static void GetSum()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Reset();
            stopWatch.Start();
            int sum = ints.Sum();
            stopWatch.Stop();
            TimeLogging(stopWatch, sum, "NoParallel");
        }

        private static void GetSumParallelFor()
        {
            Stopwatch stopWatch = new Stopwatch();
            int sum = 0;

            stopWatch.Reset();
            stopWatch.Start();
            object _lock = new object();

            List<Thread> threads = new List<Thread>();
            int rangeThread = 7;
            var array = GetPagesArr(rangeThread);

            for (int j = 0; j < array.Count; j++)
            {
                var th = new Thread((object value) =>
                {
                    for (int i = 0; i < ((List<int>)value).Count; i++)
                        lock (_lock)
                            sum += ((List<int>)value)[i];
                })
                { IsBackground = true };
                th.Start(array[j]);
                threads.Add(th);
            }

            foreach (var thread in threads)
                thread.Join();

            stopWatch.Stop();
            TimeLogging(stopWatch, sum, "Thread");
        }

        private static List<List<int>> GetPagesArr(int pages)
        {
            var pagesArr = new List<List<int>>();
            var page = new List<int>();
            var length = Math.Ceiling(ints.Length / (double)pages);
            int count = 0;
            foreach (var item in ints)
            {
                if (count < length)
                {
                    page.Add(item);
                    if (count == (length - 1))
                    {
                        pagesArr.Add(page);
                        page = new List<int>();
                        count = 0;
                    }
                    else
                        count += 1;
                }
                else
                    count = 0;
            }
            if (page.Count > 0)
                pagesArr.Add(page);
            return pagesArr;
        }

        private static async void GetSumTask()
        {
            Stopwatch stopWatch = new Stopwatch();
            int sum = 0;
            object _lock = 0;
            stopWatch.Reset();
            stopWatch.Start(); 
            for (int i = 0; i < ints.Length; i++)
                await Task.Run(() => sum += ints[i]);
            stopWatch.Stop();
            TimeLogging(stopWatch, sum, "Task.Run");
        }

        private static void GetSumParallelLinq()
        {
            Stopwatch stopWatch = new Stopwatch();
            int sum = 0;
            object _lock = 0;
            stopWatch.Reset();
            stopWatch.Start();
            ints.AsParallel().ForAll(i =>
            {
                lock (_lock)
                    sum += i;
            });
            stopWatch.Stop();
            TimeLogging(stopWatch, sum, "Parallel LINQ");
        }

        public static void TimeLogging(Stopwatch stopWatch, int sum, string typeProcess)
        {
            Console.WriteLine($"\nТип суммирования: {typeProcess}." +
                $"\nРезультат суммирования: {sum}" +
                $"\nЗатраченное время: {stopWatch.Elapsed.TotalMilliseconds}");
        }
    }
}
